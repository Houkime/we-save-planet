extends Landmark

class_name SmallHome

func _init():
	spritePath = "res://home.png"
	pavement_steps = 4
	pollution = 0