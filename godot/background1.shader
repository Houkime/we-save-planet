shader_type canvas_item;

uniform float size_x=0.008;
uniform float size_y=0.008;
uniform float phase = 0;
uniform vec4 color1 : hint_color = vec4(1.0,1.0,1.0,1.0) ;
uniform vec4 color2 : hint_color = vec4(0.0,0.0,0.0,1.0) ;

void fragment() {
	vec2 uv = SCREEN_UV;
	uv-=mod(uv,vec2(size_x,size_y));
	vec3 col = color1.rgb;
	float diag = (uv.x+uv.y)*10.0;
	if (sin(diag+phase) >=0.7){
		col = color2.rgb;}
	
	COLOR.rgb= col;
}