extends Node2D

export var viols_required = 2
var viols_found = 0
var completed = false

var masterControlPath = "../ZShifter/Themer"
onready var sub = get_node(masterControlPath+"/TopBar/Submissions")
onready var comp = get_node(masterControlPath+"/LevelComplete")



# Called when the node enters the scene tree for the first time.
func _ready():
	update_label()
	pass # Replace with function body.

func add_violation():
	if !completed:
		viols_found+=1
		update_label()
		if viols_found >= viols_required:
			completed = true
			comp.complete()

func update_label():
	sub.text = "violations found: " + str(viols_found) + "/" + str(viols_required)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
