extends Node2D

class_name Circle

var z = 1
var outline_z = z-2
var outline = null
var poly = null
var rad = 100
var startang = 0
var finishang=PI*2
var col=ColorN('white')
var outline_col = ColorN('black')
var outline_width = 2
var steps = 30
onready var anglestep = 2*PI/steps




func _ready():
	
	poly = create_subpoly()
	poly.color=col
	
	outline=create_subpoly(rad+outline_width)
	outline.color=outline_col
	
	poly.z_index=z
	outline.z_index=outline_z
	
	poly.add_child(outline)
	add_child(poly)

func create_subpoly(radius=rad):
	
	if startang==finishang:
		return
	#centerpoint
	var varang=startang
	var pool=PoolVector2Array()
	pool.push_back(Vector2(0,0))
	#arch
	while (varang<=finishang):
		pool.push_back(radius*Vector2(cos(varang),sin(varang)))
		varang=varang+anglestep
	varang=finishang
	pool.push_back(radius*Vector2(cos(varang),sin(varang)))
	var subpoly=Polygon2D.new()
	subpoly.polygon=pool
	return subpoly
	
	