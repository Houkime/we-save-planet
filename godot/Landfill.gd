extends Landmark

class_name Landfill
func _init():
	spritePath = "res://landfill.png"
	illegal_existence = true

func _ready():
	visible = false