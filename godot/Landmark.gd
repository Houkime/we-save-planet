extends Node2D

class_name Landmark

export var spritePath = "res://plant.png"
export var sprite_scale = 2

export var pollution = 0.8 #keep it float
export var illegal_existence = false
export var pavement_steps = 8 
var reported = false
var sprite = null

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.

func report():
	reported = true
	if sprite!=null:
		sprite.modulate = ColorN('orange')
	visible = true
	

func validate():
	if sprite!=null:
		sprite.modulate = ColorN('cyan')

func _ready():
	sprite = Sprite.new()
	sprite.texture = load(spritePath)
	sprite.scale = Vector2(sprite_scale,sprite_scale)
	add_child(sprite)
	sprite.z_index = 20
	var pavement = Circle.new()
	pavement.steps = pavement_steps
	pavement.rad = 30
	pavement.z = 10
	pavement.outline_z = -1
	pavement.outline_col = ColorN("white")
	pavement.col = ColorN("black")
	pavement.rotation_degrees += 360.0/16.0
	add_child (pavement)