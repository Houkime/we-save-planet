extends Label

var anitime = 0.5
#var th = load("res://theme.tres")

class_name TerminalLine

func _init(tex,col):
	#theme = th
	visible = false
	rect_min_size = Vector2(50,10)
	add_color_override("font_color", col)
	text = tex

func _ready():
	roll_in()
	
func roll_in():
	var tween =  Tween.new()
	add_child(tween)
	tween.interpolate_property(self,"percent_visible",0,1,anitime,Tween.TRANS_QUAD,Tween.EASE_IN_OUT)
	tween.start()
	visible = true
#
#func cleanup():
#	queue_free()
#	pass