extends Node2D

var levels = ["res://Level1.tscn","res://Level2.tscn","res://Level3.tscn"]
var briefings = [
	"""
	Ecological Task Force now officially has sniffer drones.
	They will be used to report plants that emit too much pollutants.
	""",
	""" 
	Sniffer drones have successfully proven their efficiency and attracted more funding for RnD.
	With enchanced H2S sensitivity second generation of ecodrones is able to sniff out hidden landfills in forests and ruins.
	""",
	"""
	Cheap means of production enabled by spreading adoption of additive technologies means that now every home 
	may easily become a busy factory without hurdle of taxation or compliance with ecological regulations.
	Government and ecologists alike though supporting technical progress, are heavily against unregulated activity which may 
	(and occasionally does) destroy local ecology.
	"""
	]
var current_level = 0
var g
var copter


func _ready():
	print ("flyscreen readying")
	prepare_level(current_level)

func prepare_level(number):
	var trash = [copter,g]
	for t in trash:
		if t!= null:
			t.queue_free()
	g = load(levels[number]).instance()
	add_child(g) 
	copter = Copter.new()
	copter.number = number
	add_child(copter)
	copter.position = Vector2(400.0,100.0)
	var brief = $ZShifter/Themer/Briefing
	brief.find_node("Label").text= "NEWS:\n"+briefings[number]
	brief.visible = true

func _next_level():
	current_level+=1
	if current_level in range(levels.size()):
		prepare_level(current_level)
	else:
		print ("YOU WON!")