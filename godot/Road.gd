extends Line2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

class_name Road

# Called when the node enters the scene tree for the first time.
func _ready():
	default_color = ColorN("white",1)
	var asphalt = Line2D.new()
	z_index = 1
	width =10
	for p in points:
		asphalt.add_point(p)
	asphalt.default_color = ColorN("black")
	asphalt.z_index = 10
	asphalt.width = width - 4
	add_child(asphalt)
	
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
