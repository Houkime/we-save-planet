shader_type canvas_item;

uniform float size_x=1;
uniform float size_y=1;

void fragment() {
	vec2 uv = SCREEN_UV*20.0/1.5;
	uv = vec2(uv.x*1024.0/600.0, -uv.y);
	uv=mod(uv,vec2(size_x,size_y));
	//uv = vec2(uv.x, -uv.y);
	
	COLOR.rgb= textureLod(TEXTURE,uv,0.0).rgb;
}
	