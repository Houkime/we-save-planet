extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var co2
var text
var repvalue = 0
var norm = 0.2
onready var thres = rand_range (norm,norm*4)
onready var rep = $Label

func _init():
	randomize()

func next_issue():
	co2=rand_range(0,0.8)
	text="CO2 "+str(co2)+" norm"+str(norm)
	$PanelContainer/RichTextLabel.text=text

func submit():
	var pop = PopupPanel.new()
	var label = Label.new()
	var suceeded = co2>thres
	label.text = "yep!" if suceeded else "nope("
	repvalue+= 1 if suceeded else -1
	pop.add_child(label)
	add_child(pop)
	pop.popup_centered()
	update_rep()
	

func update_rep():
	rep.text = "REPUTATION: "+str(repvalue)

# Called when the node enters the scene tree for the first time.
func _ready():
	next_issue()
	update_rep()
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
