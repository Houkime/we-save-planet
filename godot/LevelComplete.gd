extends PanelContainer

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var timer= null

# Called when the node enters the scene tree for the first time.

func complete():
	visible = true
	timer = Timer.new()
	timer.connect("timeout",self,'_next')
	timer.wait_time = 3
	add_child(timer)
	timer.start()

func _ready():
	visible = false
	pass # Replace with function body.

func _next():
	visible=false
	timer.queue_free()
	get_parent().get_parent().get_parent()._next_level()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
