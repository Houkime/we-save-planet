extends Polygon2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

class_name Forest

# Called when the node enters the scene tree for the first time.
func _ready():
	texture = load("res://tree.png")
	var mat = ShaderMaterial.new()
	mat.shader = load('res://screenbased_tex_multiplier.shader')
	material = mat
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
