extends Node2D

class_name Copter

var number = 0
var speed = 300
var polluters = []
var pollution = 0
var HomogenityRadius = 20
var repvalue = 0
var norm = 0.2
var HomogenityArea = PI*pow(HomogenityRadius,2.0)
onready var thres = rand_range (norm,norm*4)

var masterControlPath = "../ZShifter/Themer"
onready var rep = get_node(masterControlPath+"/TopBar/Reputation")
onready var pollution_label = get_node(masterControlPath+"/Readout/Pollution")
onready var sub_button = get_node(masterControlPath+"/Submit")


func _process(delta):
	if Input.is_action_pressed("ui_up"):
		#print("pressed")
		position= position + speed*delta*Vector2(0,-1)
		#print(position)
	if Input.is_action_pressed("ui_down"):
		#print("pressed")
		position= position + speed*delta*Vector2(0,1)
	if Input.is_action_pressed("ui_left"):
		#print("pressed")
		position= position + speed*delta*Vector2(-1,0)
	if Input.is_action_pressed("ui_right"):
		#print("pressed")
		position= position + speed*delta*Vector2(1,0)
	pollution = 0
	for p in polluters:
		var distance = (p.position - position).length()/HomogenityRadius
		pollution+=p.pollution/distance if distance > 1 else p.pollution
	#print (pollution)
	pollution_label.text = "Pollution: %.2f" % pollution

func _ready():
	print("copter readying")
	var vis = load("res://copter.tscn").instance()
	add_child(vis)
	print ("polluters contain", polluters.size())
	for c in  $"..".g.get_children():
		if c is Landmark:
			polluters.append(c)
	print ("polluters now contain", polluters.size())
	update_rep()
	sub_button.connect("button_down",self,"submit")
	vis.z_index=30

func _init():
	randomize()

func submit():
	var pop = PopupPanel.new()
	var label = Label.new()
	var dist = INF
	var target = null
	for p in polluters:
		var distance = (p.position - position).length()
		if distance < dist:
			dist = distance
			target = p
	print ("thres ", thres)
	var succeded = (pollution>thres or target.illegal_existence) and !target.reported
	if succeded:
		label.text = "yep!"
		repvalue+= 1
		target.report()
		$"..".g.add_violation()
	else:
		label.text = "nope("
		repvalue-= 1
		if !target.reported:
			target.validate() #not sure about this.
	
	pop.add_child(label)
	add_child(pop)
	pop.popup_centered()
	update_rep()

func update_rep():
	rep.text = "REPUTATION: "+str(repvalue)