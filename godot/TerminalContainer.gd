extends PanelContainer

var rolled_out = false
onready var in_tween = Tween.new()
onready var out_tween = Tween.new()
onready var pos = rect_position
onready var initpos = $initpos.global_position

func _ready():
	visible = false
	add_child(in_tween)
	add_child(out_tween)
	
func roll_in():
	var anitime = 1
	rect_position = initpos
	in_tween.interpolate_property(self,"rect_position",rect_position,pos,anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	in_tween.start()
	visible = true
	rolled_out = true

func retreat():
	var anitime = 1
	out_tween.interpolate_property(self,"rect_position",pos,initpos,anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	out_tween.start()
	out_tween.connect("tween_completed",self,"clb")
	rolled_out = false

func clb(arg1,arg2):
	print("called back")
	visible = false
	
func _process(delta):
	if Input.is_action_just_pressed("terminal"):
		if !rolled_out:
			roll_in()
		else:
			 retreat()