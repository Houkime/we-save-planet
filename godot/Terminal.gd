extends VBoxContainer

var col = ColorN("yellow")
var max_length = 60
var max_lines = 5
var lines = []

func _ready():
	add_message("hello1")
	add_message("hello2")
	add_message("hello3")
	add_message("hello4")
	add_message("hello5")
	add_message("hello6")
	add_message("hello7")
#	setup_texts("initial")
#	var timer = Timer.new()
#	timer.wait_time = 2
#	add_child(timer)
#	timer.connect("timeout",self,"speak")
#	timer.start()
#	randomize()
#	speak()

#func speak():
#	var text  = current_texts[randi() % current_texts.size()]
#	add_message(text)
	
#func setup_texts(setname):
#	current_texts = texts[setname]

func add_message(text):
	var newline = TerminalLine.new(text,col)
	add_child(newline)
	lines.append(newline)
	if lines.size()>max_lines:
		lines[0].queue_free()
		lines.remove(0)
	